<?php
    include_once 'includes/register.inc.php';
    include_once 'includes/functions.php';?>

<!DOCTYPE html>
<html>
    
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script>
        <script src="https://kit.fontawesome.com/cbfe9cca5d.js" crossorigin="anonymous"></script>
        <link rel="shortcut icon" href="Images/ampersand-full.256.png" type="favicon/ico" />
        <link rel="stylesheet" href="CSS\RegisterStyle.css">
    </head>
    
    <body>
        <?php if (!empty($error_msg)) {echo $error_msg;}?>
        <div class="container">
            <div class="signup">
                <h1 style="font-size: 45px;width: 100%;">Join us on the dark side.</h1>
                <form action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" 
                    method="post" 
                    name="registration_form">
                    <input type='text' name='username' id='username' placeholder="Username. Make it good."/><br><br>
                    <input type="text" name="email" id="email" placeholder="E-mail. So we can reach you." /><br><br>
                    <input type="password" name="password" id="password" placeholder="Password. Don't let them in."/><br><br>
                    <input type="password" name="confirmpwd" id="confirmpwd" placeholder="Password again. To be sure."/><br><br>
                    <input type="button" value="Register" onclick="return regformhash(this.form,this.form.username,this.form.email,this.form.password,this.form.confirmpwd);" /> 
                </form>
                <p>Trying to <a href="LoginPage.php" class="login">login? <i class="fas fa-sign-in-alt"></i></a></p>
                <div class="login-help">
                    <ul>
                        <li>Usernames can only contain: uppercase and lowercase letters, numbers and underscores</li>
                        <li>Passwords must contain:</li>
                        <ul>
                            <li>At least 6 characters</li>
                            <li>At least one capital letter</li>
                            <li>At least one lowercase letter</li>
                            <li>At least one number</li>
                        </ul>
                    </ul>
                </div>
            </div>
        </div>
    </body>
</html>