<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();
?>  
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    
    <head>
        <link rel="shortcut icon" href="Images/ampersand-full.256.png" type="favicon/ico" />
        <meta charset="UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="CSS\StyleProj.css">
        <script src="https://kit.fontawesome.com/cbfe9cca5d.js" crossorigin="anonymous"></script>
    </head>
    
    <body>
        <div style="top:10px;position:fixed"><?php
            include_once './includes/Menu.php';
            ?>
        </div>
        
        <div class="content">
            
            <?php if (login_check($mysqli) == true) : ?>
                <p style="font-size: 35px;">
                    Welcome back <?php echo(ucfirst(htmlentities($_SESSION['username']))); ?>!
                    <br>
                    <a href="includes/logout.php" class="logout">Sign Out <i class="fas fa-sign-out-alt"></i></a>
                </p>
            <?php else: ?>
                <p>
                    <span class="error"></span> Oh no! It seems that you have not  <a href="LoginPage.php" class="login">logged in <i class="fas fa-sign-in-alt"></i></a> yet. Quick!
                    <br> 
                    <br>
                    <span class="error"></span> Need an account? The magic happens right <a href="RegisterPage.php" class=login>here <i class="fas fa-user-plus"></i></a>!
                </p>
            <?php endif; ?>
        </div>
    </body>

</html>
