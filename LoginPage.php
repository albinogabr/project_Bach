<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';
sec_session_start();?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel="shortcut icon" href="Images/ampersand-full.256.png" type="favicon/ico" />
        <link rel="stylesheet" href="CSS\LoginStyle.css">
        <script type="text/JavaScript" src="js/sha512.js"></script> 
        <script type="text/JavaScript" src="js/forms.js"></script> 
        <script src="https://kit.fontawesome.com/cbfe9cca5d.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <?php  if (isset($_GET['error'])): ?>
            <p class="error">Oops! Looks like something went wrong. Try again!</p>
        <?php endif ?>
        <div class="container">
            <div class="login-form">
                <h1 style="font-size: 45px;width: 100%;">Get in - We have candy.</h1>
                <form method="post" action="includes/process_login.php">
                    <input type="text" name="email" placeholder="E-mail"> 
                    <br><br>
                    <input type="password" name="password" placeholder="Password" id="password">
                    <br><br>    
                    <input type="button" value="Go!" onclick="formhash(this.form, this.form.password);" />
                </form>
                <br>
                <p>Need an account? Right <a class="login" href="RegisterPage.php">here <i class="fas fa-user-plus"></i></a></p>
            </div>
        </div>
    </body>
</html>